# Presenza
The de facto way to keep track of school student's presence in online classes

## What is Presenza?
Presenza is a Discord bot that, with the help of calendars, automatically keeps track of student presence. All without any teacher intervention.
And in aim of making it as easy and careless as possible to see which students are present or not, Presenza has a web-dashboard that makes this task all to simple.

# Q&A
Q: Is Presenza finished or at least usable?<br>
A: No. Not yet. Nowhere near it. Any contribution is very welcome.

## Usage
Documentation about how to set up Presenza can be found [here](/docs/installation.md).

# Contributing
If you're in the mood to help out with some of the dashboard's Sass, here's a command you can use to properly watch it.

`npx sass --watch --no-source-map --load-path node_modules/bootstrap/scss web/static/scss/:web/static/css`

## Contact & help
Creating a issue or a pull request first of all would be appreciated, but if it's way too important for any reason, send an email to hampus@hampusmat.com.
