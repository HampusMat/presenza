/*
   Source file copied from node-ical (https://www.npmjs.com/package/node-ical)
   Author: Jens Maus
   License: Apache License, Version 2.0
*/

const ical = require('./ical.js');
const fetch = require("node-fetch");
const rrule = require('rrule');
const { DateTime } = require("luxon");

/**
 * ICal event object.
 *
 * These two fields are always present:
 *  - type
 *  - params
 *
 * The rest of the fields may or may not be present depending on the input.
 * Do not assume any of these fields are valid and check them before using.
 * Most types are simply there as a general guide for IDEs and users.
 *
 * @typedef iCalEvent
 * @type {object}
 *
 * @property {string} type           - Type of event.
 * @property {Array} params          - Extra event parameters.
 *
 * @property {?object} start         - When this event starts.
 * @property {?object} end           - When this event ends.
 *
 * @property {?string} summary       - Event summary string.
 * @property {?string} description   - Event description.
 *
 * @property {?object} dtstamp       - DTSTAMP field of this event.
 *
 * @property {?object} created       - When this event was created.
 * @property {?object} lastmodified  - When this event was last modified.
 *
 * @property {?string} uid           - Unique event identifier.
 *
 * @property {?string} status        - Event status.
 *
 * @property {?string} sequence      - Event sequence.
 *
 * @property {?string} url           - URL of this event.
 *
 * @property {?string} location      - Where this event occurs.
 * @property {?{
 *     lat: number, lon: number
 * }} geo                            - Lat/lon location of this event.
 *
 * @property {?Array.<string>}       - Array of event catagories.
 */
/**
 * Object containing iCal events.
 * @typedef {Object.<string, iCalEvent>} iCalData
 */
/**
 * Callback for iCal parsing functions with error and iCal data as a JavaScript object.
 * @callback icsCallback
 * @param {Error} err
 * @param {iCalData} ics
 */
/**
 * A Promise that is undefined if a compatible callback is passed.
 * @typedef {(Promise.<iCalData>|undefined)} optionalPromise
 */

// Utility to allow callbacks to be used for promises
function promiseCallback(fn, cb)
{
	const promise = new Promise(fn);
	if(!cb){
		return promise;
	}

	promise
	.then(returnValue =>
	{
		cb(null, returnValue);
	})
	.catch(error =>
	{
		cb(error, null);
	});
}

/**
 * Parse iCal data from a string.
 *
 * @param {string} data       - String containing iCal data.
 * @param {icsCallback} [cb]  - Callback function.
 *                              If no callback is provided a promise will be returned.
 *
 * @returns {optionalPromise} Promise is returned if no callback is passed.
 */
parseICS = function(data, cb)
{
	return promiseCallback((resolve, reject) =>
	{
		ical.parseICS(data, (error, ics) =>
		{
			if(error){
				reject(error);
				return;
			}

			resolve(ics);
		});
	}, cb);
};

function getRRule(cal)
{
	var new_rrule = new rrule.RRuleSet();
	new_rrule.rrule(new rrule.RRule(cal["rrule"]["options"]));
	if(cal["exdate"]){
		for(exdate in cal["exdate"]){
			var d = cal["exdate"][exdate];
			d = DateTime.fromJSDate(d).toUTC().setZone('local', { keepLocalTime: true }).toJSDate();
			new_rrule.exdate(d);
		}
	}
	return new_rrule;
}

async function getFromURL(url)
{
   let request;
   try{
      request = await fetch(url);
   }
   catch{
      return;
   }
	var content = await request.text();
	return await parseICS(content);
}

// Export api functions
module.exports = {
	getFromURL,
	getRRule,
	// Other backwards compat things
	objectHandlers: ical.objectHandlers,
	handleObject: ical.handleObject,
	parseLines: ical.parseLines
};
