const { Sequelize, DataTypes } = require("sequelize");
const db = require("../config/db");

const users = db.define("web_users", {
	uid: {
		type: DataTypes.INTEGER,
		allowNull: false,
		autoIncrement: true,
		primaryKey: true
	},
	username: {
		type: DataTypes.TEXT,
		allowNull: false,
	},
	password: {
		type: DataTypes.TEXT,
		allowNull: false,
	},
	is_admin: {
		type: DataTypes.BOOLEAN,
		allowNull: false
	}
});

const sessions = db.define("web_sessions", {
	sid: {
		type: DataTypes.STRING(36),
		allowNull: false,
		primaryKey: true
	},
	expires: {
		type: DataTypes.DATE
	},
	data: {
		type: DataTypes.JSONB,
		allowNull: false
	}
});

module.exports.users = users;
module.exports.sessions = sessions;