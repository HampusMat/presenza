const { DataTypes } = require("sequelize");
const db = require("../config/db");
const { schema } = require("./config");

const groups = db.define("groups", {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true
    },
    calendar: {
        type: DataTypes.STRING,
        allowNull: false
    },
    students: {
        type: DataTypes.JSONB,
        allowNull: false
    }
});

const role_specific_classes = db.define("role_specific_classes", {
    role: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true
    },
    class: {
        type: DataTypes.STRING,
        allowNull: false
    },
    default: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

const days = db.define("days", {
    date: {
        type: DataTypes.STRING,
        allowNull: false,
		primaryKey: true
    }
});

const day_template = {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    class: {
        type: DataTypes.STRING,
        allowNull: false
    },
    group: {
        type: DataTypes.STRING,
        allowNull: false
    },
    announced: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    start: {
        type: DataTypes.DATE,
        allowNull: false
    },
    end: {
        type: DataTypes.DATE,
        allowNull: false
    },
    presence: {
        type: DataTypes.JSONB,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    }
};

module.exports.groups = groups;
module.exports.days = days;
module.exports.role_specific_classes = role_specific_classes;
module.exports.day_template = day_template;