const { DataTypes } = require("sequelize");
const db = require("../config/db");

const config = db.define("config", {
	key: {
		type: DataTypes.STRING,
		allowNull: false,
		primaryKey: true
	},
	value: {
		type: DataTypes.STRING,
		allowNull: false,
	}
});

module.exports = config;