/*
	Script that compiles all the necessary Sass for the dashboard.
*/

const sass = require('sass');
const fs = require("fs");

const path_in = "./web/static/scss/";
const path_out = "./web/static/css/";
const includes = ["./node_modules/bootstrap/scss"];

function compileSass(file, out){
	fs.writeFileSync(path_out + out, sass.renderSync({
		file: path_in + file,
		outputStyle: "compressed",
		includePaths: includes
	}).css);
	console.log(`Successfully compiled ${file}!`);
}

compileSass("login.scss", "login.css")
compileSass("style.scss", "style.css")
console.log("Done!");