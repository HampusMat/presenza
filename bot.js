const discord = require('discord.js');
const express = require("express");
const { Sequelize, Op } = require("sequelize");
const db = require("./config/db");
const website = require("./models/website");
const school = require("./models/school");
const config = require("./models/config");
const session = require("express-session");
const path = require('path');
const cookieParser = require("cookie-parser");
const body = require("./web/middleware/body");
const { I18n } = require("i18n");
const argon2 = require("argon2");
const ical = require('./misc/calendar');
const { differenceInMilliseconds, format, isAfter, isBefore } = require("date-fns");
const { DateTime } = require("luxon");
var SequelizeStore = require("./misc/sequelize-sessions")(session.Store);
require("dotenv").config();

// Initialize Discord API connection & Express
const client = new discord.Client();
const app = express();

// Discord
var prefix;
var guild;
var language;
var guild_allowed;
var channel;

// Language support
var i18n;

// Main-logic loop
// - Creates new tables for new school days
// Emits the event for when a class has begun
async function mainLoop()
{
    const now = new Date();
    const date = format(now, "yyyyMMdd");
    const day_classes = db.define(date, school.day_template, { schema: "classes" });
    let classes;
    try{
        classes = await day_classes.findAll();
    }
    catch{
        // A table for the current day is missing
        console.log(`${i18n.__("log-creating-schoolday")} ${date}`);
        let groups = await school.groups.findAll();
        let group_cnt = groups.length;

        console.log(group_cnt);

        if(group_cnt === 0 || typeof group_cnt !== "number" || isNaN(group_cnt)){
            console.log("OOPS");
            return;
        }
        // Go through all the groups and add their classes to the table
        groups.forEach(async (group_data, group_index) =>
		{
            group_data = group_data.dataValues;
            const calendar = group_data.calendar;
            const group = group_data.name;
            const students = group_data.students;
            
            // Get the group's calender and go through it
            const cal = await ical.getFromURL(calendar);
			if(!cal){
				console.log(`${i18n.__("log-download-cal-err")} ${calendar}!`);
				return;
			}
            let cal_index = 0;
            for(let item in cal){
                // Make sure the event is the type of event we want
                if(cal[item]["type"] !== "VEVENT" || !cal[item].hasOwnProperty("rrule") || cal[item]["start"].getHours() < 8) continue;

                // Make sure the event is today and is a valid class
                const rrules = ical.getRRule(cal[item]);
                const dates = rrules.all().map(date =>
                    DateTime.fromJSDate(date)
                      .toUTC()
                      .setZone('local', { keepLocalTime: true })
                      .toJSDate()
                    );
                const valid_class = /^\w+(?=.*\bgrp(\d)\b)|(^(?!Resurstid)(MP1|.*(?=\ \[)))/;
                const class_parts = valid_class.exec(cal[item]["summary"]);
                const class_date_today = dates.find(d => {
                    if(d.getUTCFullYear() === now.getUTCFullYear() && 
                        d.getUTCMonth() === now.getUTCMonth() && 
                        d.getUTCDate() === now.getUTCDate() && 
                        class_parts !== null
                    ) return true;
                });
                if(!class_date_today) continue;

                // Actually create the table for the current day.
                // This is so that tables doesn't get created for days without any classes
                try{
                    await day_classes.sync();
                }
                catch(err){
                    throw err;
                }
                console.log(`${i18n.__("log-class")} ${class_parts[0]}`);

				let class_group;
                if(class_parts[1]){
					class_group = class_parts[1];
				}

                // Create presence data for the day.
                // This also takes into account role-specific classes.
                const role_class = (await school.role_specific_classes.findAll({ where: { [Op.or]: [ { class: class_parts[0] }, { default: class_parts[0] } ] } }))[0];
                if(role_class) console.log(JSON.stringify(role_class));
                let presence = {};
                for(let group in students["groups"]){
                    if((class_group && group === class_group) || !class_group){
                        presence[group] = {};
                        students["groups"][group].forEach(student => {
                            if((role_class && class_parts[0] === role_class["class"] && student[1] && student[1].some(role => role === role_class["role"]))
                                || (role_class && class_parts[0] !== role_class["class"] && class_parts[0] === role_class["default"] && (!student[1] || (student[1] && !student[1].some(role => role === role_class["role"]))))
                                || (!role_class)
                            ){
                                presence[group][student[0]] = ["false", ""];
                            }
                        });
                    }
                }
                // Add all of the data to the day and add the date of the day to a table with school days
                await day_classes.create({
                    class: class_parts[0],
                    group: group,
                    announced: false,
                    start: format(class_date_today, "yyyy-MM-dd HH:mm:ss"),
                    end: format(new Date(class_date_today.getTime() + differenceInMilliseconds(cal[item]["end"], cal[item]["start"])), "yyyy-MM-dd HH:mm:ss"),
                    presence: presence,
                    password: ""
                })
                    .catch(err =>
                    {
                        console.log(`${i18n.__("log-create-class-err-1")} ${cal[item]["summary"]} ${i18n.__("log-create-class-err-2")} ${date}`);
                        return;
                    });

                await school.days.findOrCreate({ where: { date: format(now, "yyyyMMdd") } })
                    .catch(err =>
                    {
                        console.log(`${i18n.__("log-create-schooldays-entry-err")} ${date}`);
                        return;
                    });
                cal_index = cal_index + 1;
            }
        });
        classes = null;
    }
    // If there exists a table for the current day, emit the class_begin event if a class has begun
    if(classes){
        delete classes.meta;
        for(var class_name in classes){
            var start = classes[class_name]["start"];
            if(start.getUTCFullYear() === now.getUTCFullYear() && start.getUTCMonth() === now.getUTCMonth() && start.getUTCDate() === now.getUTCDate() && start.getHours() === now.getHours() && start.getMinutes() === now.getMinutes() && classes[class_name]["announced"] === 0){
	            const out = guild.channels.cache.get(channel);
	            out.send(`${class_name} ${i18n.__("class-has-begun")}`);
                classes[class_name]["announced"] = true;
                classes.sync();
            }
        }
    }
    
    await new Promise(resolve => setTimeout(resolve, 1000));
    mainLoop();
}

// When the Discord API is ready
client.once("ready", async () =>
{
    const guilds = client.guilds.cache;
	guild = guilds.get(guild_allowed);
	if(guild === null || isNaN(guild) || !guild.available){
		throw `${i18n.__("log-guild-not-available-err")} ${guild_allowed}`;
	}
	console.log(`${i18n.__("log-ready")} ${guild.name}.`);

	// Start Expess
	app.set("view engine", "ejs");
	app.set('trust proxy', 1)
	app.set("views", "./web/views");
	app.use("/res", express.static("web/static/res"));
	app.use("/css", express.static("web/static/css"));
	app.use("/img", express.static("web/static/img"));
	app.use("/js/presenza", express.static("web/static/js"));
	app.use("/js/bootstrap", express.static("node_modules/bootstrap/dist/js"));
	app.use("/js/chartjs", express.static("node_modules/chart.js/dist/"));

	// Prepare the session and user databases
	website.sessions.sync();
	const session_store = new SequelizeStore({
		db: db,
		table: "web_sessions",
		expiration: 604800000
	});
	website.users.sync();
	session_store.sync();

	app.use(cookieParser());
	app.use(i18n.init);

	// Get request body and session information
	app.use(body.parse);
	app.use(session({
		secret: process.env.SESSION_SECRET,
		store: session_store,
		resave: false,
		saveUninitialized: false,
		cookie: { sameSite: "strict", maxAge: 604800000 }
	}));

	const router = require("./web/routes/router");
	const api_router = require("./web/routes/api");

	app.use("/", api_router);
	app.use("/", router);
	app.listen((await config.findOne({ attributes: ["value"], where: { key: "port" }}))["value"]);
	console.log(i18n.__("log-started-dashboard"));
});

client.on("message", async (message) =>
{
	if (message.author.bot || message.system ) return;
	if (!message.content.startsWith(prefix)) return;
  
	const body = message.content.slice(prefix.length);
	const args = body.split(' ');
	const command = args.shift().toLowerCase();

	if(message.guild.id !== guild.id || (message.channel.type === "dm" && !guild.members.cache.has(message.author.id))){
		return;
	}
	
	const student = guild.member(message.author).displayName;

	// Sets the presence for the author student
	if(command === "here"){
		const now = new Date();
		const date = format(now, "yyyyMMdd");
		const day_classes = db.define(date, school.day_template, { schema: "classes" });
		let current_class;
		let student_group;
	    current_class = await day_classes.findAll()
            .catch(err =>
			{
                console.log(err);
                return;
            });
		current_class = current_class.find(class_data =>
		{
			if(isAfter(now, class_data["start"]) && isBefore(now, class_data["end"])){
				for(const group in class_data["presence"]){
					if(Object.keys(class_data["presence"][group]).find(name => name === student)){
						student_group = group;
						return true;
					}
                }
			}
		});
		if(!current_class || !student_group){
			console.log(`${i18n.__("log-no-class-found")} ${student}`);
			return;
		}
		const password = message.content.slice(prefix.length + command.length + 1);
		if(current_class["password"] && message.channel.type !== "dm"){
			message.delete({ reason: i18n.__("message-delete-reason") });
			message.reply(i18n.__("class-has-password"));
			return;
		}
		if(current_class["password"] && ! await argon2.verify(current_class["password"], password)){
			message.reply(i18n.__("class-password-invalid"));
			return;
		}

		// Find the student and set the presence
		let presence = current_class.presence;
		if(presence[student_group][student][0] === "true"){
			return;
		}
		presence[student_group][student] = ["true", now];
		
		try{
			await day_classes.update({ presence: presence }, { where: { id: current_class.id }});
			await day_classes.sync();
			console.log(`${student} ${i18n.__("log-student-present-1")} ${current_class.class} ${i18n.__("log-student-present-2")} ${now.toString()}`);
			message.reply(`${i18n.__("you-are-present")} ${current_class.class}`);
		}
		catch{
			return;
		}
	}
});

config.addHook("afterUpdate", (cfg, options) =>
{
	if(cfg["key"] === "prefix"){
		prefix = cfg["value"];
	}
	if(cfg["key"] === "guild"){
		guild = cfg["value"];
	}
});

async function initialize()
{
	i18n = new I18n({
		locales: ['en', 'sv'],
		defaultLocale: "en",
		cookie: "lang",
		directory: path.join(__dirname, 'locales')
	});
	// Make sure the database is fine and dandy
	try{
		await db.authenticate();
		await db.sync();
	}
	catch{
		console.log(`${i18n.__("log-missing-database")} ${process.env.DB_NAME}`);
		const repair = new Sequelize("", process.env.DB_USERNAME, process.env.DB_PASSWORD, db.config);
		const repair_query = repair.getQueryInterface();
		await repair_query.createDatabase(process.env.DB_NAME);
		await repair.close();

		try{
			await db.authenticate();
			await db.sync();
		}
		catch{
			throw i18n.__("log-database-err");
		}

		await db.createSchema("classes");
	}

	// Default configuration
	await config.sync();
	await config.bulkCreate([
		{ key: "port", value: "8080" },
		{ key: "logo", value: "/img/mtu-logo.svg" },
		{ key: "guild", value: "809012047731228702" },
		{ key: "channel", value: "809012047731228705" },
		{ key: "prefix", value: "!" },
		{ key: "language", value: "en" }
	])
		.catch(() => { });
	
	// Apply configuration to a couple of variables
	const configuration = await config.findAll({ where: {
		[Op.or]: [
			{ key: "prefix" },
			{ key: "guild" },
			{ key: "channel" },
			{ key: "port" },
			{ key: "language" }
		]
	}});

	prefix = configuration.find(cfg => cfg["key"] === "prefix")["value"];
	port = configuration.find(cfg => cfg["key"] === "port")["value"];
	guild_allowed = configuration.find(cfg => cfg["key"] === "guild")["value"];
	channel = configuration.find(cfg => cfg["key"] === "channel")["value"];
	language = configuration.find(cfg => cfg["key"] === "language")["value"];

	i18n = new I18n({
		locales: ['en', 'sv'],
		defaultLocale: language,
		cookie: "lang",
		directory: path.join(__dirname, 'locales')
	});

	// Create an administrator user if missing
	website.users.findOrCreate({ where: { is_admin: true }, defaults: { username: "admin", password: await argon2.hash("admin") }, raw: true })
		.then(user => {
			if(user[1] === true ) console.log(`${i18n.__("log-admin-account-missing")} admin:admin`);
		})
		.catch(() => {
			throw i18n.__("log-create-admin-err");
		});

	mainLoop();

	client.login(process.env.DISCORD_TOKEN);
}

initialize();
