**Table of contents:**

[[_TOC_]]

# Setting up the discord bot
You'll need a [Discord developer portal](https://discord.com/developers/applications) application.

Go to there and click the 'New Application' button.

![](/docs/screenshots/installation/1.png)

Then, enter what you want to call your application (Presenza for example) and click the 'Create' button.

![](/docs/screenshots/installation/2.png)

You'll now want to click the 'Copy' button under the application's client id. You need this so save it somewhere for later.

![](/docs/screenshots/installation/3.png)
 
Now, go to the 'Bot' tab.

![](/docs/screenshots/installation/4.png)

And click the 'Add bot' button to create a bot.

![](/docs/screenshots/installation/5.png)

Make sure you press the popup's 'Yes, do it!' button.

![](/docs/screenshots/installation/6.png)

And then click the 'Copy' button under the bot's token. You need this so save it somewhere for later. 
Also, make sure you never tell anyone your token. It's very secret!

![](/docs/screenshots/installation/7.png)

You'll probably also want to make sure that you're the only one who can use your bot.

![](/docs/screenshots/installation/8.png)
![](/docs/screenshots/installation/9.png)
And click the 'Save Changes'

![](/docs/screenshots/installation/10.png)

You can now invite your Discord bot by going to

`https://discord.com/oauth2/authorize?client_id=(CLIENT ID)&scope=bot&permissions=76800`

Replace `CLIENT ID` with the client id you copied from the Discord developer portal.

# Prerequities
(If you're not planning to use Docker)
- [Nodejs](https://nodejs.org/en/download/current/)
- [Postgresql](https://www.postgresql.org/download/)

# Setting up Presenza
First of all, clone the git repository.

`$ git clone https://gitlab.com/HampusMat/presenza.git`

And go in there

`$ cd presenza`

Now, create a file called `.env` that contains the following.
```
DB_NAME=(Name of the database to use)
DB_HOST=(Hostname of postgresgl server)
DB_PORT=(Port of postgresql server)
DB_USERNAME=(Postgresql username)
DB_PASSWORD=(Postgresql password)
DISCORD_TOKEN=(Bot token you copied from the Discord developer portal)
SESSION_SECRET(A random string used to create sessions ids for the web-dashboard)
```

There are multiple ways to continue

1. With Docker (Recommended)
2. Manually

### Docker 
There is an example shell script in `docs/examples/` that builds & starts a Presenza container along with a Postgresql container.<br>
You can run it with:

`./docs/examples/docker.sh`

### Manually
If you're using Presenza for the first time, run the following command to install all the required dependencies.

`$ npm install`

You'll also need to compile the dashboard's Sass. Do this by running

`$ npm run build`

Start Presenza with

`$ node bot.js`
