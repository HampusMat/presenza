#!/bin/bash
docker stop postgres presenza
docker rm postgres presenza
docker volume rm postgres
docker image rm presenza

docker build -t presenza .

docker run --name postgres -d -v postgres:/var/lib/postgresql/data -e POSTGRES_PASSWORD=postgres -p 5432:5432 --network presenza postgres
sleep 2
docker run -d -v $(pwd):/app -e TZ=$(find /usr/share/zoneinfo/ -type f| xargs md5sum | grep $(md5sum /etc/localtime  | cut -d' ' -f1) | grep -Po '(?<=\/usr\/share\/zoneinfo\/)(?!posix).*') --name presenza -p 8080:8080 --network presenza presenza