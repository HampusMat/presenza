const { Sequelize } = require("sequelize");
const fs = require('fs'); 
const env_path = fs.realpath(`${process.cwd()}/..`, (err, path) => { return path });
require("dotenv").config({ path: env_path });

console.log();

const config = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "postgres",
    logging: false,
    define: {
        timestamps: false,
        freezeTableName: true
    }
};

module.exports = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, config);
module.exports.config = config;