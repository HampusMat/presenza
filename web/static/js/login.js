function setUsernameIconFocus(state)
{
    if(state === 1){
        document.styleSheets[0].addRule('#label-username::before','background: url("/img/login/user-sel.svg") center / contain no-repeat;');
    }
    else {
        document.styleSheets[0].addRule('#label-username::before','background: url("/img/login/user.svg") center / contain no-repeat;');
    }
}

function setPasswordIconFocus(state)
{
    if(state === 1){
        document.styleSheets[0].addRule('#label-password::before','background: url("/img/login/lock-sel.svg") center / contain no-repeat;');
    }
    else {
        document.styleSheets[0].addRule('#label-password::before','background: url("/img/login/lock.svg") center / contain no-repeat;');
    }
}

function SubmitLogin()
{
    let xhr = new XMLHttpRequest(); 
    xhr.open('POST', `/api/v1/login`);
    xhr.setRequestHeader('Content-type', 'application/json');
    const form = `{"username":"${document.getElementById("username").value}","password":"${document.getElementById("password").value}"}`;
    xhr.send(form);

    xhr.onload = function()
    {
        if (xhr.status != 200) {
            alert(`Error ${xhr.status}: ${xhr.statusText}`);
        } 
        else {
            const response = JSON.parse(xhr.responseText);
            if(response["status"] === "ok"){
                const params = new URLSearchParams(window.location.search);
                const to = params.get("to");
                if(to){
                    window.location.replace(to);
                }
                else{
                    window.location.replace("/");
                }
            }
            else{
                const password_fail_div = document.getElementById("password-fail");
                password_fail_div.innerHTML = "Ogiltigt användarnamn eller lösenord!";
            }
        }
    };

    xhr.onerror = function()
    {
        alert("Request failed");
    };

    return false;
}

function switchLanguage(lang)
{
    document.cookie = `lang=${lang};maxAge=3155760000;sameSite=lax`;
    location.reload();
    return false;
}

$(document).ready(function(){
    $('[data-bs-toggle="tooltip"]').tooltip();
});