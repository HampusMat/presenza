function request(method, source, data = null)
{
	return new Promise(function (resolve, reject){
		let xhr = new XMLHttpRequest(); 
		xhr.open(method, source, true);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.send(data);

		xhr.onload = function()
		{
			if(this.status >= 200 && this.status < 300){
				resolve(xhr.response);
			}
			resolve({ status: this.status, statusText: xhr.statusText });
		};
		xhr.onerror = () =>
		{
			resolve({ status: this.status, statusText: xhr.statusText });
		}
	});
}

async function logout()
{
	const response = JSON.parse(await request("POST", "/api/v1/logout"));
	if(response["status"] === "ok"){
		window.location.replace("/");
		return;
	}
	console.error("Failed logging out.");
}

function submitClassPassword(now, id, class_name)
{
	const pass = document.getElementById("class-password").value;
	async function send(){
		let result = await request("POST", `/api/v1/presence/${now}/${id}/password`, `{"password": "${pass}"}`);
		if(typeof result !== "object"){
			result = JSON.parse(result);
		}
		if(result["status"] === "ok"){
			createAlert(`Successfully set the password for ${class_name}`, "alert-success");
			return;
		}
		createAlert("Error: failed to set the password for the class!", "alert-danger");
	}
	send();
	return false;
}

async function updateContent(from)
{
	const content = await request("GET", `/content/${from}`);
	document.getElementById("content").innerHTML = content;

	document.getElementById(content_where).classList.remove("selected");
	document.getElementById(from).classList.add("selected");

	if(from === "home"){
		let groups = await request("GET", "/api/v1/groups");
		groups = JSON.parse(groups)["data"];
		await initializeDropdown(groups, "select-group-dd", "group-dd-button");
		await subgroupDropdown();
		await whenDropdowns();
	}

	const from_title = from.charAt(0).toUpperCase() + from.substring(1);
	document.title = `Presenza - ${from_title}`;
	window.history.pushState({}, `Presenza - ${from_title}`, from);
	content_where = from;
}

async function initializeDropdown(data, dd_id, item_id, is_subgroups = false)
{
	const ul = document.getElementById(dd_id);	
	let li = document.createElement('li');
	console.log(data);
	data.forEach(item =>
	{
		let _item;
		if(typeof item === "object"){
			if(item["name"]){
				_item = item["name"];
			}
		}
		else{
			if(is_subgroups){
				_item = `Grupp ${item}`;
			}
			else{
				_item = item;
			}
			
		}
		if(data.indexOf(item) === 0){
			setDropdownSelector(item_id, _item);
		}
		item = _item;
		console.log(item);
		const a = document.createElement('a');
		a.setAttribute("class", "dropdown-item");
		a.setAttribute("onclick", `return setDropdownSelector('${item_id}', '${item}')`);
		a.appendChild(document.createTextNode(item));
		li.appendChild(a);

		while(ul.lastChild){
			ul.removeChild(ul.lastChild);
		}
		ul.appendChild(li);
	});
}

async function subgroupDropdown()
{
	let subgroups = await request("GET", `/api/v1/groups/${document.getElementById("group-dd-button").innerHTML}`);
	subgroups = JSON.parse(subgroups);
	subgroups =  Object.keys(subgroups["data"][0]["students"]["groups"]);
	await initializeDropdown(subgroups, "select-subgroup-dd", "subgroup-dd-button", true);
}

async function whenDropdowns()
{
	let schooldays = await request("GET", "/api/v1/presence");
	schooldays = JSON.parse(schooldays)["data"];
	let days = {};
	schooldays.forEach(date =>
	{
		const year = date["date"].substr(0,4);
		const month = date["date"].substr(4,2);
		const day = date["date"].substr(6,2);

		if(!days[year]){
			days[year] = {};
			days[year][month] = [];
			days[year][month].push(day);
			return;
		}
		else if(!days[year][month]){
			days[year][month] = [];
			days[year][month].push(day);
			return;
		}
		else if(!days[year][month].find(d => d === day)){
			days[year][month].push(day);
		}
	});
	console.log(JSON.stringify(days));
	await initializeDropdown(Object.keys(days), "select-when-year-dd", "when-year-dd-button");
	const year = document.getElementById("when-year-dd-button").innerHTML;
	await initializeDropdown(Object.keys(days[year]), "select-when-month-dd", "when-month-dd-button");
	const month = document.getElementById("when-month-dd-button").innerText;
	await initializeDropdown(days[year][month], "select-when-day-dd", "when-day-dd-button");
}


async function setDropdownSelector(id, value)
{
	document.getElementById(id).innerHTML = value;
	if(id === "group-dd-button"){
		await subgroupDropdown();
	}
}

function toggleSidepanel(media_query = null)
{
	if(media_query){
		if(media_query.matches){
			document.getElementById("panel").classList.add("collapse");
			document.getElementById("show-panel").classList.remove("hide");
			return;
		}
		document.getElementById("panel").classList.remove("collapse");
		document.getElementById("show-panel").classList.add("hide");
		return;
	}
	
	document.getElementById("panel").classList.toggle("collapse");
	document.getElementById("show-panel").classList.toggle("hide");
}

function createAlert(text, alert_style){
	const base = document.getElementById("alert");
	const alert = document.createElement("div");
	alert.classList.add("alert", alert_style, "alert-dismissible", "fade", "show");
	alert.setAttribute("role", "alert");
	alert.appendChild(document.createTextNode(text));

	const dismiss_button = document.createElement("button");
	dismiss_button.setAttribute("type", "button");
	dismiss_button.setAttribute("data-bs-dismiss", "alert");
	dismiss_button.setAttribute("aria-label", "Close");
	dismiss_button.classList.add("btn-close");

	alert.appendChild(dismiss_button);
	base.appendChild(alert);
}

document.addEventListener("DOMContentLoaded", async function ()
{
	const min_width_sidepanel = window.matchMedia("(max-width: 808px)");
	toggleSidepanel(min_width_sidepanel);
	min_width_sidepanel.addEventListener("change", toggleSidepanel);

	document.getElementById(content_where).classList.add("selected");
	const content = await request("GET", `/content/${content_where}`);
	document.getElementById("content").innerHTML = content;

	let groups = await request("GET", "/api/v1/groups");
	groups = JSON.parse(groups)["data"];
	await initializeDropdown(groups, "select-group-dd", "group-dd-button");
	await subgroupDropdown();
	await whenDropdowns();

	const year = document.getElementById("when-year-dd-button").innerHTML;
	const month = document.getElementById("when-month-dd-button").innerHTML;
	const day = document.getElementById("when-day-dd-button").innerHTML;

	const classes = JSON.parse(await request("GET", `/api/v1/presence/${year}${month}${day}`));
	
	let getStatistics = new Promise((resolve, reject) =>
	{
		let data = {};
		classes.data.forEach(async class_ =>
		{
			const classdata = JSON.parse(await request("GET", `/api/v1/presence/${year}${month}${day}/${class_['id']}`));
			for(const [group, students] of Object.entries(classdata["data"]["presence"])){
				if(data[group] === undefined){
					data[group] = [0, Object.entries(students).length];
				}
				for(const [student, student_data] of Object.entries(students)){
					console.log(`${group} ${student} ${student_data[0]}`)
					if(student_data[0] === "true"){
						data[group][0]++;
					}
				}
			}
			if(classes.data.indexOf(class_) === classes.data.length -1) resolve(data);
		});
	});

	getStatistics.then(data =>
	{
		console.log(data)
		/*for(const [group, stats] of Object.entries(data)){
			data[group] = (stats[0] / stats[1]);
		}
		console.log(data)*/
	});

	var ctx = document.getElementById('myChart');
	var myChart = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
			datasets: [{
				label: '# of Votes',
				data: [12, 19, 3, 5, 2, 3],
				backgroundColor: [
					'rgba(255, 99, 132, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)'
				],
				borderColor: [
					'rgba(255, 99, 132, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)'
				],
				borderWidth: 1
			}]
		}
	});
});