const argon2 = require("argon2");
const db = require("./../../config/db");
const website = require("./../../models/website");
const school = require("./../../models/school");
const config = require("./../../models/config");
const express = require("express");
const { parseAsync } = require('json2csv');
const fs = require('fs');
const router = express.Router();
const error = require("./../util/err");
const db_fetch = require("./../util/db-fetch");

// Endpoint for trying to authorize.
// Sets a cookie and redirects to / if it succeeds
router.post("/api/v1/login", async function(req, res)
{
	if(!req.body || !req.body.username || !req.body.password){
		res.json({ status: "fail" });
	}
	else{
		const db_data = await website.users.findAll({ attributes: ["password", "is_admin"], where: { username: req.body.username } });
		if(!db_data || db_data.length === 0 || db_data[0].length === 0){
			res.json({ status: "access denied" });
		}
		else{
			const cmp = await argon2.verify(db_data[0]["password"], req.body.password);
			if(cmp){
				req.session.username = req.body.username;
				req.session.is_admin = db_data[0]["is_admin"];
				res.json({ status: "ok" });
			}
			else{
				res.json({ status: "access denied" });
			}
		}
	}
});

// Make sure whoever calls the api is authorized
router.all("/api/v1/*", async function(req, res, next)
{
	if(req.session.username){
		next();
	}
	else{
		res.json({ status: "access denied" });
	}
});

// Endpoint for logging out
router.post("/api/v1/logout", async function(req, res, next)
{
	res.clearCookie("connect.sid");
	req.session.destroy(() =>
	{
		res.json({ status: "ok" });
	});
});

// Endpoint for getting a list of all the groups
router.get("/api/v1/groups", async function(req, res, next)
{
	try{
		const data = await school.groups.findAll({ attributes: ["name"] });
		res.json({ status: "ok", data: data });
	}
	catch{
		res.json({ status: "fail" });
	}
});

// Endpoint for getting a group
router.get("/api/v1/groups/:group", async function(req, res, next)
{
	try{
		const data = await school.groups.findAll({ where: { name: req.params.group } });
		if(data.length != 0){
			res.json({ status: "ok", data: data });
		}
		else{
			res.json({ status: "fail" });
		}
	}
	catch{
		res.json({ status: "fail" });
	}
});

// Endpoint for creating a group
router.post("/api/v1/groups/:group", async function(req, res)
{
	if(await error.validateNewGroup(res, req)){
		school.groups.create({
			name: req.params.group,
			calendar: req.body.calendar,
			students: req.body.students
		})
			.then(() => {
				res.json({ status: "ok" });
			})
			.catch(() => {
				res.json({ status: "fail" });
			});
	}
});

// Endpoint for deleting a group
router.delete("/api/v1/groups/:group", async function(req, res, next)
{
	try{
		await (await school.groups.findOne( { where: { name: req.params.group } })).destroy();
		res.json({ status: "ok" });
	}
	catch{
		res.json({ status: "fail" });
	}
});

// Endpoint for getting school days
router.get("/api/v1/presence", async function(req, res)
{
	try{
		const data = await school.days.findAll();
		if(data.length != 0){
			res.json({ status: "ok", data: data });
		}
		else{
			res.json({ status: "fail" });
		}
	}
	catch{
		res.json({ status: "fail" });
	}
});

// Endpoint for getting all the classes of a school day
router.get("/api/v1/presence/:date", async function(req, res)
{
	db_fetch.schoolday(res, "find", req.params.date, { attributes: ["id", "class", "group"] });
});

// Endpoint for getting the presence in a class of a school day
router.get("/api/v1/presence/:date/:classid", async function(req, res)
{
	db_fetch.schoolday(res, "findOne", req.params.date, { where: { id: req.params.classid } });
});

// Endpoint for setting the password of a class
router.post("/api/v1/presence/:date/:classid/password", async function(req, res)
{
	if(!req.body.password){
		res.json({ status: "fail", data: "Missing password" });
	}
	else{
		db_fetch.schoolday(res, "update", req.params.date, { where: { id: req.params.classid } }, { password: await argon2.hash(req.body.password) });
	}
});

// Admin endpoints require administrator priviliages
router.all("/api/v1/admin/*", async function(req, res, next)
{
	if(req.session.is_admin){
		next();
	}
	else{
		res.json({ status: "access denied" });
	}
});

// Endpoint for getting all users
router.get("/api/v1/admin/users", async function(req, res)
{
	const users = await website.users.findAll({ attributes: ["uid", "username", "is_admin"] });
	res.json({ status: "ok", data: users});
});

// Endpoint for creating a user
router.post("/api/v1/admin/users/:user", async function(req, res)
{
	if(req.body.password && req.body.is_admin){
		try{
			await website.users.create({
				username: req.params.user,
				password: await argon2.hash(req.body.password),
				is_admin: req.body.is_admin
			});
		}
		catch(err) {
			res.json({ status: "fail", data: err });
			res.end();
		}
		res.json({ status: "ok" });
	}
	else{
		res.json({ status: "fail", data: "Insufficient user data supplied" });
	}
});

// Endpoint for deleting a user
router.delete("/api/v1/admin/users/:user", async function(req, res)
{
	try{
		await website.users.destroy({ where: { username: req.params.user } });
		await website.sessions.destroy({ where: { "data.username": req.params.user } });
		res.json({ status: "ok" });
	}
	catch(err) {
		res.json({ status: "fail", data: err });
	}
});

// Endpoint for exporting all groups to a csv
router.get("/api/v1/admin/groups/export", async function(req, res)
{
	await school.groups.findAll({ raw: true })
		.then(groups =>
		{
			groups.forEach(row => { row["students"] = JSON.stringify(row["students"]) });
			res.setHeader("Content-Type", "text/csv");
			parseAsync(groups)
				.then(csv_data =>
				{
					console.log(csv_data);
					res.send(csv_data);
				})
				.catch(() =>
				{
					res.json({ status: "fail" });
				});
			
		})
		.catch(function ()
		{
			res.json({ status: "fail" });
		});
});

// Endpoint for importing groups from a csv
router.post("/api/v1/admin/groups/import", async function(req, res)
{
	if(!req.body){
		res.json({ status: "fail", data: "No or invalid file sent" });
	}
	else{
		let failed = false;
		req.body.forEach(row =>
		{
			let parsed;
			try{
				parsed = JSON.parse(row["students"]);
			}
			catch{
				res.json({ status: "fail" });
				failed = true;
			}
			if(parsed && !error.validateStudentsData(res, parsed)){
				res.json({ status: "fail", data: "Invalid students data" });
				failed = true
			}
			row["students"] = parsed;
		});

		if(failed === false){
			await school.groups.bulkCreate(req.body)
			.then(response =>
			{
				res.json({ status: "tjena", data: response });
			})
			.catch(() =>
			{
				res.json({ status: "fail", data: "Error bulk creating group data" });
			});
		}
	}
});

// Endpoint for getting all configuration keys and values
router.get("/api/v1/admin/config", async function(req, res)
{
	try{
		const data = await config.findAll();
		res.json({ status: "ok", data: data});
	}
	catch{
		res.json({ status: "fail" });
	}
});

// Endpoint for getting the value of a configuration key
router.get("/api/v1/admin/config/:key", async function(req, res)
{
	res.json(await async function()
	{
		try{
			const data = (await config.findOne({ attributes: ["value"], where: { key: req.params.key } }))["value"];
			return { status: "ok", data: data };
		}
		catch{
			return { status: "fail" };
		}
	}());
});

// Endpoint for setting the value of a configuration key
router.post("/api/v1/admin/config/:key", async function(req, res)
{
	res.json(await async function()
	{
		if(!req.body.value){
			return { status: "fail", data: "Missing value to set the configuration key to." };
		}
		try{
			await config.update({ value: req.body.value }, { where: { key: req.params.key }, individualHooks: true });
			return { status: "ok" };
		}
		catch{
			return { status: "fail" };
		}
	}());
});

// Endpoint for deleting a schoolday table
// It will be automatically created again, however
router.delete("/api/v1/admin/presence/:date", async function(req, res)
{
	const verify_date = /^\d{4}(0[1-9]|1[0-2])([0-2]\d|3[0-1])$/;
	if(!verify_date.test(req.params.date)){
		res.json({ status: "fail", data: "invalid date" });
		return;
	}
    db_fetch.schoolday(res, "drop", req.params.date);
});

router.get("/api/v1/*", function(req, res)
{
	res.json({ status: "fail", data: "404: Page not found" });
});

module.exports = router;
