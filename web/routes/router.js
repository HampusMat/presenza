const db = require("./../../config/db");
const school = require("./../../models/school");
const config = require("./../../models/config");
const api_data = require("./../util/api-data");
const express = require("express");
const { format } = require("date-fns");
require("dotenv").config({ path: "./../../" });
const router = express.Router();

// Login page
router.get("/login", async function(req, res){
	// Check if there even is a need to log in
	if(req.session.username){
		res.redirect("/");
	}
	else{
		// Find out if the request is from an failed login attempt
		let data = {};
		if(!req.query.hasfailed){
			data = { hasfailed: false };
		}
		else{
			data = { hasfailed: req.query.hasfailed };
		}
		res.render("login", data);
	}
});

// Verify session
router.use(async function (req, res, next){
	if(req.session.username){
		next();
	}
	else{
		res.redirect(`/login?to=${req.path}`);
	}
});

// Home
router.get("/", async function(req, res){
	res.redirect("/home");
});
router.get("/home", async function(req, res){
	const logo = (await config.findOne({ attributes: ["value"], where: { key: "logo" } }))["value"];
	res.render("base", { where: "home", logo: logo });
});
router.get("/content/home", async function(req, res){
	const active_classes = await api_data.getActiveClasses(new Date());
	res.render("content/home", { username: req.session.username, active_classes: active_classes, now: format(new Date(), "yyyyMMdd")});
});

// Groups
router.get("/groups", async function(req, res, next){
	const data = (await config.findOne({ attributes: ["value"], where: { key: "logo" } }))["value"];
	res.render("base", { where: "groups", logo: data });
});
router.get("/content/groups", async function(req, res, next){
	let data;
	try{
		data = await school.groups.findAll();
	}
	catch{
		next();
	}
	res.render("content/groups", { data: data });
});

// Group information
router.get("/groups/:group", async function(req, res, next){
	let data;
	try{
		data = await school.groups.findAll({
			where: {
				name: req.params.group
			}
		});
	}
	catch{
		next();
	}
	if(data) res.render("group", { group: req.params.group, data: data });
});

// List of days with presence information
router.get("/presence", async function(req, res, next){
	const data = (await config.findOne({ attributes: ["value"], where: { key: "logo" } }))["value"];
	res.render("base", { where: "presence", logo: data });
});

router.get("/content/presence", async function(req, res, next){
	let days;
	try{
		days = await school.days.findAll();
	}
	catch{
		next();
	}
    let days_data = {};
	for(day in days){
		days_data[days[day]["date"]] = "hej";
	}
	res.render("content/presence", { days: days_data });
});

// List of classes in day
router.get("/presence/:date", async function(req, res, next){
    const day_classes = db.define(`classes_${req.params.date}`, school.day_template);
    let classes;
    try{
        classes = await day_classes.findAll({ attributes: ["id", "class", "group", "start", "end"] });
    }
    catch{
        next();
    }
    if(classes) res.render("classes", { date: req.params.date, classes: classes });
});

// Presence information for a class of a day
router.get("/presence/:date/:class_id", async function(req, res, next){
	const day_classes = db.define(`classes_${req.params.date}`, school.day_template);
    let class_data;
    try{
        class_data = await day_classes.findAll({ where: { id: req.params.class_id } });
    }
    catch{
        next();
    }
    if(class_data) res.render("class", { class_data: class_data });
});

// 404
router.use(function(req, res) {
	res.status(404).send('404: Page not Found');
});

module.exports = router;