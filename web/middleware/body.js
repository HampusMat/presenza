const parse_csv = require("fast-csv");

// Read the request buffer and put it all into a single variable
function compileBody(req, res)
{
	return new Promise(function(resolve, reject)
	{
		let body = [];
		req.on("error", err =>
		{
			res.json({ status: "fail", data: `Error reading body stream. ${err.stack}` });
			resolve();
		}).on("data", chunk =>
		{
			body.push(chunk);
		}).on("end", () =>
		{
			body = Buffer.concat(body).toString();
			resolve(body);
		});
	});
}

// Parse the request body and put it inside the request
async function parse(req, res, next)
{
	if(req.method === "POST"){
		if(req.is('application/json')){
			const body = await compileBody(req, res);
			try{
				req.body = JSON.parse(body);
				next();
			}
			catch(err){
				res.json({ status: "fail", data: `Invalid JSON data. ${err}` });
			}
		}
		else if(req.is('text/csv')){
			const body = await compileBody(req, res);
			const json_data = [];
			parse_csv.parseString(body, { headers: true })
				.on("error", error =>
				{
					res.json({ status: "fail", data: "Error parsing csv data" });
				})
				.on("data", data =>
				{
					json_data.push(data);
				})
				.on("end", async () =>
				{
					req.body = json_data;
					next();
				})
		}
		else{
			next();
		}
	}
	else{
		next();
	}
}

module.exports.parse = parse;