const db = require("./../../config/db");
const school = require("./../../models/school");
const error = require("./err");

function schoolday(res, query_type, date, options = {}, values = {})
{
    const day_classes = db.define(date, school.day_template, { schema: "classes" });

    let query;
    if(query_type === "find"){
        query = day_classes.findAll(options);
    }
    if(query_type === "findOne"){
        query = day_classes.findOne(options);
    }
    if(query_type === "update"){
        query = day_classes.update(values, options);
    }
    if(query_type === "drop"){
        query = day_classes.drop();
    }

    query
        .then(data =>
        {
            if(!data || data.length != 0){
                res.json({ status: "ok", data: data });
                return;
            }
            res.json({ status: "fail", data: error.getSequelizeErrorCodeMessage("42P01") });
        })
        .catch(err =>
        {
            console.log(err);
            res.json({ status: "fail", data: error.getSequelizeErrorCodeMessage(err["parent"]["code"]) });
        });
}

module.exports.schoolday = schoolday;