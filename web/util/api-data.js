const db = require("./../../config/db");
const school = require("./../../models/school");
const { format, isAfter, isBefore } = require("date-fns");

async function getActiveClasses(date)
{
    const day_classes = db.define(format(new Date(), "yyyyMMdd"), school.day_template, { schema: "classes" });
    const classes = await day_classes.findAll();
    const active_classes = classes.filter(_class =>
    {
        if(isAfter(date, _class["start"]) && isBefore(date, _class["end"])){
            return true;
        }
    });
    return active_classes;
}
async function getPresence(date)
{
    const day_classes = db.define(format(new Date(), "yyyyMMdd"), school.day_template, { schema: "classes" });
    const classes = await day_classes.findAll({ attributes: ["id"]});
    console.log(classes);
}

module.exports.getActiveClasses = getActiveClasses;
module.exports.getPresence = getPresence;