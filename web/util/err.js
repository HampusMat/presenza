const school = require("./../../models/school");

function getSequelizeErrorCodeMessage(code)
{
    if(code === "42P01"){
        return "not found";
    }
    if(code === "22P02"){
        return "invalid class id";
    }
    return "";
}

function validateStudentsData(res, data)
{
	if(!data["groups"] || typeof data["groups"] !== "object" || Object.keys(data["groups"]).length === 0){
		res.json({ status: "fail", data: "Invalid students data" });
		return false;
	}
	for(const group in data["groups"]){
		if(isNaN(Number(group)) || !Array.isArray(data["groups"][group])){
			res.json({ status: "fail", data: "Invalid students data" });
			return false;
		}
		if(data["groups"][group].find(student =>
			{
				if(!Array.isArray(student) ||
					student.length === 0 ||
					student.length > 2 ||
					typeof student[0] !== "string" ||
					( student.length === 2 && ( typeof student[1] !== "object" || student[1].find(special => typeof special !== "string") ))
				){
					return true;
				}
			}
		)){
			res.json({ status: "fail", data: "Invalid students data" });
			return false;
		}
	}
	return true;
}

async function validateNewGroup(res, req)
{
    if(!req.body.calendar && !req.body.students){
        res.json({ status: "fail", data: "Insufficient group data supplied" });
        return false;
    }

    const group_exists = await school.groups.findOne({ where: { name: req.params.group } })
        .catch(() => {
			res.json({ status: "fail", data: "Failed to validate the given group" });
        });
	if(group_exists && group_exists.length !== 0){
		res.json({ status: "fail", data: "Group already exists" });
		return false;
	}

    const valid_calendar_url = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)\.ics/;
    if(!valid_calendar_url.test(req.body.calendar)){
		res.json({ status: "fail", data: "Invalid calendar url" });
		return false;
	}

	if(validateStudentsData(res, req.body.students)){
		return true;
	}
}

module.exports.getSequelizeErrorCodeMessage = getSequelizeErrorCodeMessage;
module.exports.validateStudentsData = validateStudentsData;
module.exports.validateNewGroup = validateNewGroup;