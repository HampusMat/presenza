#!/bin/sh
[ ! -d /app/node_modules ] && cp -r /tmp/modules/* /app
npm run build
chown -R node:node /app
node bot.js