FROM node:15.10.0-buster

ARG NODE_ENV 
ENV NODE_ENV $NODE_ENV

COPY package*.json /tmp/modules/
WORKDIR /tmp/modules
RUN npm install

WORKDIR /app

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
CMD /docker-entrypoint.sh
